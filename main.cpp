/*
 * Copyright (C) 2013 Igalia S.L.
 *
 * Author: Philippe Normand <philn@igalia.com>
 *
 * This file may be used under the terms of the GNU General Public
 * License version 2, a copy of which is found in LICENSE included in the
 * packaging of this file.
 */

#include <stdio.h>
#include <stdlib.h>

#include <icecc/comm.h>
#include <glib.h>
#include <gio/gunixinputstream.h>

using namespace std;

static int max_jobs = 0;

void handle_stats(Msg* msg) {
    MonStatsMsg* statsMsg = dynamic_cast<MonStatsMsg*>(msg);
    if (!statsMsg)
        return;

    gchar** lines = g_strsplit(statsMsg->statmsg.c_str(), "\n", 7);
    for (int i=0; i < 7; i++) {
        gchar** keyvalue = g_strsplit(lines[i], ":", 2);
        if (!g_strcmp0(keyvalue[0], "MaxJobs"))
            max_jobs += atoi(keyvalue[1]);
        g_strfreev(keyvalue);
    }
    g_strfreev(lines);
}

gboolean handle_activity(MsgChannel* scheduler) {
    Msg* m = scheduler->get_msg();
    if (!m)
        return FALSE;

    switch (m->type) {
    case M_MON_STATS:
        handle_stats(m);
        break;
    default:
        break;
    }
    delete m;
    return TRUE;
}

gboolean handle_message(GIOChannel* source, GIOCondition condition, gpointer data) {
    MsgChannel* scheduler = (MsgChannel*) data;
    while (!scheduler->read_a_bit() || scheduler->has_msg())
        if (!handle_activity(scheduler))
            break;
    return TRUE;
}

void scheduler_found(MsgChannel* scheduler) {
    scheduler->setBulkTransfer();
    scheduler->send_msg(MonLoginMsg());

    GIOChannel* channel = g_io_channel_unix_new(scheduler->fd);
    g_io_add_watch(channel, G_IO_IN, handle_message, scheduler);
}

gboolean check_scheduler(gpointer data) {
    DiscoverSched* discover = (DiscoverSched*) data;
    MsgChannel* scheduler = discover->try_get_scheduler();
    if (!scheduler)
        return TRUE;
    scheduler_found(scheduler);
    return FALSE;
}

gboolean exit(gpointer data) {
    GMainLoop* loop = (GMainLoop*) data;
    if (!max_jobs)
        max_jobs = sysconf(_SC_NPROCESSORS_ONLN);
    g_print("%d\n", max_jobs);
    g_main_loop_quit(loop);
    return FALSE;
}

int main(int argc, char** argv) {
    DiscoverSched* discover = new DiscoverSched("ICECREAM");
    g_timeout_add(10, check_scheduler, (gpointer) discover);

    GMainLoop* loop = g_main_loop_new(NULL, FALSE);
    g_timeout_add_seconds(1, exit, loop);

    g_main_loop_run(loop);
    return 0;
}
