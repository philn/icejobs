This is a small utility program able to scan an Icecream compilation
network and retrieve the number of compile jobs it can handle.

This information is useful when invoking make:

::

   make -j`icejobs`

