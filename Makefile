
CXXFLAGS := -O2 `pkg-config --cflags glib-2.0 gio-unix-2.0 icecc`
LDFLAGS := -ldl `pkg-config --libs glib-2.0 gio-unix-2.0 icecc`
SOURCES := main.cpp

all: icejobs

%.o:: %.cpp
	$(CXX) $(CXXFLAGS) -o $@ -c $<

icejobs: $(patsubst %.cpp, %.o, $(SOURCES))
	$(CXX) $^ $(LDFLAGS) -o $@

clean:
	rm -f icejobs *.o

install: icejobs
	install icejobs ~/.local/bin/
